from handlers.pull_requests import get_pull_requests
import responses


@responses.activate
def test_get_bug():
    link_url = 'https://api.github.com/search/issues?q='
    query_string = 'state%3Aopen+label%3Abug+is%3Apr+repo%3Aboto%2Fboto3&per_page=100'
    responses.get(
        url=link_url + query_string,
        json={
            "items": [
                {
                    "html_url": "https://github.com/boto/boto3/pull/3008",
                    "number": 3008,
                    "title": "Add composite alarms (cloudwatch resources)",
                    "labels": [
                        {
                            "name": "bug"
                        }
                    ],
                    "state": "open"
                }
            ]
        }
    )

    assert get_pull_requests('bug') == \
        [{'link': 'https://github.com/boto/boto3/pull/3008',
          'num': 3008,
          'title': 'Add composite alarms (cloudwatch resources)'}]


@responses.activate
def test_get_review():
    link_url = 'https://api.github.com/search/issues?q='
    query_string = 'state%3Aopen+label%3Aneeds-review+is%3Apr+repo%3Aboto%2Fboto3&per_page=100'
    responses.get(
        url=link_url + query_string,
        json={
            "items": [
                {
                    "html_url": "https://github.com/boto/boto3/pull/3008",
                    "number": 3008,
                    "title": "Add composite alarms (cloudwatch resources)",
                    "labels": [
                        {
                            "name": "needs-review"
                        }
                    ],
                    "state": "open"
                }
            ]
        }
    )

    assert get_pull_requests('needs-review') == \
        [{'link': 'https://github.com/boto/boto3/pull/3008',
          'num': 3008,
          'title': 'Add composite alarms (cloudwatch resources)'}]


@responses.activate
def test_get_open():
    responses.get(
        url='https://api.github.com/repos/boto/boto3/pulls?state=open&per_page=100',
        json=[
            {
                "html_url": "https://github.com/boto/boto3/pull/3008",
                "number": 3008,
                "title": "Add composite alarms (cloudwatch resources)",
                "labels": [
                    {
                        "name": "needs-review"
                    }
                ],
                "state": "open"
            }
        ]
    )

    assert get_pull_requests('open') == \
        [{'link': 'https://github.com/boto/boto3/pull/3008',
          'num': 3008,
          'title': 'Add composite alarms (cloudwatch resources)'}]


@responses.activate
def test_get_closed():
    responses.get(
        url='https://api.github.com/repos/boto/boto3/pulls?state=closed&per_page=100',
        json=[
            {
                "html_url": "https://github.com/boto/boto3/pull/3008",
                "number": 3008,
                "title": "Add composite alarms (cloudwatch resources)",
                "labels": [
                    {
                        "name": "needs-review"
                    }
                ],
                "state": "closed"
            }
        ]
    )

    assert get_pull_requests('closed') == \
        [{'link': 'https://github.com/boto/boto3/pull/3008',
          'num': 3008,
          'title': 'Add composite alarms (cloudwatch resources)'}]
